<?php


namespace Fastapi\Qywx;

class Department  extends BaseQwApi
{

    /**获取部门列表
     *access_token	是	调用接口凭证
     *  id	否	部门id。获取指定部门及其下的子部门（以及及子部门的子部门等等，递归）。 如果不填，默认获取全量组织架构
     * */
    public function getList($id = ''){
        $url = "https://qyapi.weixin.qq.com/cgi-bin/department/list?access_token={$this->getToken()}&id=$id";
        $res = Http::get($url);
        if (isset($res->errcode) && $res->errcode==0){
            return $res->department;
        }else{
            return [];
        }
    }
    /**
     * 获取部门详情
     * $id  部门id
     * */
    public function info($id){
        $url = "https://qyapi.weixin.qq.com/cgi-bin/department/get?access_token={$this->getToken()}&id=$id";
        $res = Http::get($url);
        if (isset($res->errcode) && $res->errcode==0){
            return $res->department;
        }else{
            return false;
        }

    }

    /**
     * 获取部门成员列表
     * department_id	是	获取的部门id
     * fetch_child	否	是否递归获取子部门下面的成员：1-递归获取，0-只获取本部门
     *
     * */

    public function simplelist($department_id,$fetch_child=0){
        $url = "https://qyapi.weixin.qq.com/cgi-bin/user/simplelist?access_token={$this->getToken()}&department_id=$department_id&fetch_child=$fetch_child";
        $res = Http::get($url);
        if (isset($res->errcode) && $res->errcode==0){
            return $res->userlist;
        }else{
            return [];
        }
    }

    /**
     * 获取部门成员详情
     * department_id	是	获取的部门id
     * fetch_child	否	是否递归获取子部门下面的成员：1-递归获取，0-只获取本部门
     *
     * */

    public function userList($department_id,$fetch_child=0){

        $url = "https://qyapi.weixin.qq.com/cgi-bin/user/list?access_token={$this->getToken()}&department_id=$department_id&fetch_child=$fetch_child";
        $res =Http::get($url);
        if (isset($res->errcode) && $res->errcode==0){

            return $res->userlist;
        }else{
            return false;
        }
    }


    /**
     * name	是	部门名称。同一个层级的部门名称不能重复。长度限制为1~32个字符，字符不能包括:*?"<>｜
    name_en	否	英文名称。同一个层级的部门名称不能重复。需要在管理后台开启多语言支持才能生效。长度限制为1~32个字符，字符不能包括:*?"<>｜
    parentid	是	父部门id，32位整型
    order	否	在父部门中的次序值。order值大的排序靠前。有效的值范围是[0, 2^32)
    id	否	部门id，32位整型，指定时必须大于1。若不填该参数，将自动生成id
     *
     * */
    public function addDepar($name,$parentid,$name_en = null,$order=null,$id = null,&$res){
        $url = "https://qyapi.weixin.qq.com/cgi-bin/department/create?access_token={$this->getToken()}";
        $data = [
            'name'=>$name,
            'parentid'=>$parentid,
        ];
        if ($name_en){$data['name_en'] = $name_en;}
        if ($order){$data['order'] = $order;}
        if ($id){$data['id'] = $id;}

        $res = Http::post($url,$data);
        if (isset($res->errcode) && $res->errcode==0){
            return true;
        }else{
            return false;
        }
    }

    /**
     * name	是	部门名称。同一个层级的部门名称不能重复。长度限制为1~32个字符，字符不能包括:*?"<>｜
    name_en	否	英文名称。同一个层级的部门名称不能重复。需要在管理后台开启多语言支持才能生效。长度限制为1~32个字符，字符不能包括:*?"<>｜
    parentid	是	父部门id，32位整型
    order	否	在父部门中的次序值。order值大的排序靠前。有效的值范围是[0, 2^32)
    id	否	部门id，32位整型，指定时必须大于1。若不填该参数，将自动生成id
     *
     * */
    public function updateDepar($id,$parentid= null,$name=null,$name_en = null,$order=null,&$res){
        $url = "https://qyapi.weixin.qq.com/cgi-bin/department/update?access_token={$this->getToken()}";
        $data = [
            'id'=>$id,
            'parentid'=>$parentid,
        ];
        if ($name){$data['name'] = $name;}
        if ($parentid){$data['parentid'] = $parentid;}
        if ($name_en){$data['name_en'] = $name_en;}
        if ($order){$data['order'] = $order;}
        $res = Http::post($url,$data);
        if (isset($res->errcode) && $res->errcode==0){
            return true;
        }else{
            return false;
        }
    }


    public function delDepar($id,&$res=null){
        $url = "https://qyapi.weixin.qq.com/cgi-bin/department/delete?access_token={$this->getToken()}&id={$id}";
        $res = Http::get($url);
        if (isset($res->errcode) && $res->errcode==0){
            return true;
        }else{
            return false;
        }
    }



}