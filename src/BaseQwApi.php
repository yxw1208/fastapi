<?php


namespace Fastapi\Qywx;


class BaseQwApi
{
    protected $assess_token;

    protected $secret = null;

    public function __construct($secret = null)
    {
        $this->assess_token = (new Token($secret))->getAccessToken();
        $this->corpid = Config::CORPID;
        if (empty($secret)){
            $this->secret =  Config::SECRET;
        }else{
            $this->secret = $secret;
        }

    }
    public function getToken()
    {
        return (new Token($this->secret))->getAccessToken();
    }
}