<?php


namespace Fastapi\Qywx;


/**
 * 微信客服的相关操作
 * */
class Kefu extends BaseQwApi
{
    /**
     *获取客户列表
     * $userid  企业成员的userid
     */
    public function getMsgList($parasm=[],$cursor=null,$limit = 1000,$is_all=true,&$res=null)
    {
        $url = "https://qyapi.weixin.qq.com/cgi-bin/kf/sync_msg?access_token={$this->getToken()}";
        $data = [
            'limit'=>$limit
        ];
        $token = $parasm['token'] ?? null;
        $open_kfid = $parasm['open_kfid'] ?? null;
        if ($token){$data['token'] = $token;}
        if ($open_kfid){$data['open_kfid'] = $open_kfid;}
        if ($cursor){$data['cursor'] = $cursor;}

        $res = Http::post($url,$data);
        $code = $res->errcode ?? 1;
        if ($code == 0) {
            $next_cursor = $res->next_cursor ?? null;
            $has_more = $res->has_more ?? 0; // 是否还有更多数据。0-否；1-是。
            $msg_list = $res->msg_list ?? [];
            if ($has_more == 1){
                $s_msg_list = $this->getMsgList($token,$next_cursor,$limit);
                return array_merge($msg_list,$s_msg_list);
            }else{
                return $msg_list;
            }
        } else {
            return false;
        }
    }
}