<?php


namespace Fastapi\Qywx;


class Approval  extends BaseQwApi
{


    /**
     * 获取审批模板详情
     * template_id	是	模板ID。
     * */
    public function gettemplatedetail($template_id){
        $url = 'https://qyapi.weixin.qq.com/cgi-bin/oa/gettemplatedetail?access_token='.$this->getToken();
        $res = Http::post($url,['template_id'=>$template_id]);
        if ($res->errcode==0){
            return $res;
        }
        return false;
    }

    /**
     * 提交审批申请
     * template_id	是	模板ID。
     * */
    public function applyevent($data,&$res=null){
        $url = 'https://qyapi.weixin.qq.com/cgi-bin/oa/applyevent?access_token='.$this->getToken();
        $res = Http::post($url,$data);
        if ($res->errcode==0){
            return $res;
        }
        return false;
    }

}