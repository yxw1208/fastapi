<?php


namespace Fastapi\Qywx;


/**
 * 客户的相关操作
 * */
class Customer extends BaseQwApi
{

    /**
     * 获取群聊列表
     *
    status_filter  客户群跟进状态过滤。
    0 - 所有列表(即不过滤)
    1 - 离职待继承
    2 - 离职继承中
    3 - 离职继承完成
    默认为0
    owner_filter	否	群主过滤。
    如果不填，表示获取应用可见范围内全部群主的数据（但是不建议这么用，如果可见范围人数超过1000人，为了防止数据包过大，会报错 81017）
    owner_filter.userid_list	否	用户ID列表。最多100个
    cursor	否	用于分页查询的游标，字符串类型，由上一次调用返回，首次调用不填
    limit	是	分页，预期请求的数据量，取值范围 1 ~ 1000
     * */
    public function getGroupchatList($limit=20,$cursor=null,$status_filter=0,$is_all=false,&$res=null)
    {
        $url = "https://qyapi.weixin.qq.com/cgi-bin/externalcontact/groupchat/list?access_token={$this->getToken()}";
        $data = [
            'status_filter'=>$status_filter,
            'limit'  =>$limit,
            'cursor'  =>$cursor,
        ];
        $res = Http::post($url,$data);
        if (isset($res->errcode) && $res->errcode==0){
            $chat_list = $res->group_chat_list ?? [];
            if ($is_all){
                $next_cursor = $res->next_cursor ?? null;
                if (!$next_cursor){
                    return  $chat_list;
                }
                return array_merge($chat_list,$this->getGroupchatList($limit,$next_cursor,$status_filter,$is_all));
            }else{
                return $chat_list;
            }
        }else{
            return  [];
        }

    }

    /**
     * 获取客户群详情
     * @param $chat_id
     * @param $need_name int 是否需要返回群成员的名字group_chat.member_list.name。0-不返回；1-返回。默认不返回
     * @return false
     */
    public function getGroupchatInfo($chat_id,$need_name=1,&$res=null)
    {
        $url = "https://qyapi.weixin.qq.com/cgi-bin/externalcontact/groupchat/get?access_token={$this->getToken()}";
        $data = [
            'chat_id'  =>$chat_id,
            'need_name'=>$need_name
        ];
        $res = Http::post($url,$data);
        if (isset($res->errcode) && $res->errcode==0){
            return  $res->group_chat;
        }else{
            return  false;
        }
    }

    /**
     *获取客户列表
     * $userid  企业成员的userid
     */
    public function lists($userid)
    {

        $url = "https://qyapi.weixin.qq.com/cgi-bin/externalcontact/list?access_token={$this->getToken()}&userid=$userid";
        $res = Http::get($url);
        if ($res->errcode == 0) {
            return $res->external_userid;
        } else {
            return false;
        }
    }
    /**
     *获取客户详情
     * external_userid    是    外部联系人的userid，注意不是企业成员的帐号
     */
    public function get($external_userid)
    {

        $url = "https://qyapi.weixin.qq.com/cgi-bin/externalcontact/get?access_token={$this->getToken()}&external_userid=$external_userid";
        $res = Http::get($url);
        if (isset($res->errcode) && $res->errcode == 0) {
            return $res;
        } else {
            return false;
        }
    }
    /**
     * 批量获取客户详情
     * @param string    $userid 是	企业成员的userid，字符串类型
     * @param callable  $callacle  回调处理每个会员
     * @param string    $userid 否	用于分页查询的游标，字符串类型，由上一次调用返回，首次调用可不填
     * @param int       $userid 否	返回的最大记录数，整型，最大值100，默认值50，超过最大值时取最大值
     * */
    public function get_by_user($userid,callable $callacle=null,$cursor=null,$limit=50){
        $url = "https://qyapi.weixin.qq.com/cgi-bin/externalcontact/batch/get_by_user?access_token={$this->getToken()}";
        $data = [
            "userid"=>$userid,
            "cursor"=>$cursor,
            "limit"=>$limit
        ];
        $res = Http::post($url,$data);
        if (isset($res->errcode) && $res->errcode==0){
            if (empty($callacle)){
                return $res;
            }
            $list = $res->external_contact_list;
            if (!empty($list)){
                foreach ($list as $vip){
                    $callacle($vip);
                }
                if (isset($res->next_cursor) && !empty($res->next_cursor)){
                    return $this->get_by_user($userid,$callacle,$res->next_cursor,$limit);
                }else{
                    return $res;
                }
            }
        }else{
            return false;
        }
    }
    /**
     * remark  修改客户备注信息
     * access_token    是    调用接口凭证
     * userid    是    企业成员的userid
     * external_userid    是    外部联系人userid
     * remark    否    此用户对外部联系人的备注，最多20个字符
     * description    否    此用户对外部联系人的描述，最多150个字符
     * remark_company    否    此用户对外部联系人备注的所属公司名称，最多20个字符
     * remark_mobiles    否    此用户对外部联系人备注的手机号
     * remark_pic_mediaid    否    备注图片的mediaid，
     *
     * eg
     * {
     * "userid":"zhangsan",
     * "external_userid":"woAJ2GCAAAd1asdasdjO4wKmE8Aabj9AAA",
     * "remark":"备注信息",
     * "description":"描述信息",
     * "remark_company":"腾讯科技",
     * "remark_mobiles":[
     * "13800000001",
     * "13800000002"
     * ],
     * "remark_pic_mediaid":"MEDIAID"
     * }
     * */
    public function remark($data)
    {
        $url = "https://qyapi.weixin.qq.com/cgi-bin/externalcontact/remark?access_token={$this->getToken()}";
        $res = Http::post($url, $data);
        return $res;
    }
    /**
     *  发送新客户欢迎语
     * 企业微信在向企业推送添加外部联系人事件时，会额外返回一个welcome_code，企业以此为凭据调用接口，即可通过成员向新添加的客户发送个性化的欢迎语。
     * 为了保证用户体验以及避免滥用，企业仅可在收到相关事件后20秒内调用，且只可调用一次。
     * 如果企业已经在管理端为相关成员配置了可用的欢迎语，则推送添加外部联系人事件时不会返回welcome_code。
     * 每次添加新客户时可能有多个企业自建应用/第三方应用收到带有welcome_code的回调事件，但仅有最先调用的可以发送成功。后续调用将返回41051（externaluser has started chatting）错误，请用户根据实际使用需求，合理设置应用可见范围，避免冲突。
     *
     *
     *
     *
     *
     * */
    public function welcome($data)
    {
        $url = "https://qyapi.weixin.qq.com/cgi-bin/externalcontact/send_welcome_msg?access_token={$this->getToken()}";
        $res = Http::post($url, $data);
        return $res;
    }
    /**
     * 获取待分配的离职成员列表
    $all 是否获取所有的
    access_token	是	调用接口凭证
    page_id	否	分页查询，要查询页号，从0开始
    page_size	否	每次返回的最大记录数，默认为1000，最大值为1000
    cursor	否	分页查询游标，字符串类型，适用于数据量较大的情况，如果使用该参数则无需填写page_id，该参数由上一次调用返回
    注意:当page_id为1，page_size为100时，表示取第101到第200条记录。
    page_id和page_size参数仅适用于记录数小于五万条的情况,即 page_id*page_size < 50000；
    如果记录数大于五万，则需要使用cursor参数。
     * */
    public function get_unassigned_list($all=false,$cursor='')
    {
        $list = [];
        $url = "https://qyapi.weixin.qq.com/cgi-bin/externalcontact/get_unassigned_list?access_token={$this->getToken()}";
        $res = Http::post($url, [
            'cursor'=>$cursor
        ]);
        if (isset($res->errcode) && $res->errcode == 0 ){

            if ($all){
                $list = $res->info ?? [];
                if ($res->is_last){
                    return  $list;
                }
                return array_merge($list,$this->get_unassigned_list(true,$res->next_cursor));
            }else{
                return $res->info;
            }
        }else{
            return  [];
        }


    }
    /**
     * 分配离职成员客户
     *
    参数	必须	说明
    access_token	是	调用接口凭证
    handover_userid	是	原跟进成员的userid
    takeover_userid	是	接替成员的userid
    external_userid	是	客户的external_userid列表，最多一次转移100个客户

    handover_userid必须是已离职用户。
    external_userid必须是handover_userid的客户（即配置了客户联系功能的成员所添加的联系人）。
     * */
    public function transfer_customer($handover_userid,$new_userid,$external_userid)
    {
        $url = "https://qyapi.weixin.qq.com/cgi-bin/externalcontact/resigned/transfer_customer?access_token={$this->getToken()}";
        $num = 100;
        if (count($external_userid) > $num){
            $list = array_chunk($external_userid,$num);
            $res = [];
            $data = [
                'handover_userid'=>$handover_userid,
                'takeover_userid'=>$new_userid,
            ];
            foreach ($list as $userid){
                $data['external_userid']=$userid;
                $res[] = Http::post($url, $data);
            }
            return $res;
        }else{
            $data = [
                'handover_userid'=>$handover_userid,
                'takeover_userid'=>$new_userid,
                'external_userid'=>$external_userid
            ];
            $res = Http::post($url, $data);
            return $res;
        }



    }
    /**
     * 分配在职成员的客户
     *
    参数	必须	说明
    access_token	是	调用接口凭证
    handover_userid	是	原跟进成员的userid
    takeover_userid	是	接替成员的userid
    external_userid	是	客户的external_userid列表，每次最多分配100个客户
    transfer_success_msg	否	转移成功后发给客户的消息，最多200个字符，不填则使用默认文案
     *
     * 为保障客户服务体验，90个自然日内，在职成员的每位客户仅可被转接2次。
     * */
    public function externalcontact_transfer_customer($external_userid,$handover_userid,$takeover_userid,$transfer_success_msg='')
    {
        $num = 100;
        if (count($external_userid) > $num){
            $list = array_chunk($external_userid,$num);
            $res = [];
            $data=[
                'handover_userid'=>$handover_userid,
                'takeover_userid'=>$takeover_userid,
                'transfer_success_msg'=>$transfer_success_msg
            ];
            foreach ($list as $userid){
                $data['external_userid']=$userid;
                $url = "https://qyapi.weixin.qq.com/cgi-bin/externalcontact/transfer_customer?access_token={$this->getToken()}";
                $res[] = Http::post($url, $data);
            }
            return $res;
        }else{
            $data=[
                'handover_userid'=>$handover_userid,
                'takeover_userid'=>$takeover_userid,
                'external_userid'=>$external_userid,
                'transfer_success_msg'=>$transfer_success_msg
            ];
            $url = "https://qyapi.weixin.qq.com/cgi-bin/externalcontact/transfer_customer?access_token={$this->getToken()}";
            $res = Http::post($url, $data);
            return $res;
        }


    }
    /**
     * 获取「联系客户统计」数据
     *

    参数	必须	说明
    access_token	是	调用接口凭证
    userid	否	成员ID列表，最多100个
    partyid	否	部门ID列表，最多100个
    start_time	是	数据起始时间
    end_time	是	数据结束时间
     *
     *
     *userid和partyid不可同时为空;
    此接口提供的数据以天为维度，查询的时间范围为[start_time,end_time]，即前后均为闭区间，支持的最大查询跨度为30天；
    用户最多可获取最近180天内的数据；
    当传入的时间不为0点时间戳时，会向下取整，如传入1554296400(Wed Apr 3 21:00:00 CST 2019)会被自动转换为1554220800（Wed Apr 3 00:00:00 CST 2019）;
    如传入多个userid，则表示获取这些成员总体的联系客户数据。
     * */
    public function get_user_behavior_data(array $userid=[],array $partyid=[],$start_time,$end_time)
    {
        $url = "https://qyapi.weixin.qq.com/cgi-bin/externalcontact/get_user_behavior_data?access_token={$this->getToken()}";
        $res = Http::post($url, [
            'userid'=>$userid,
            'partyid'=>$partyid,
            'start_time'=>$start_time,
            'end_time'=>$end_time,
        ]);
        if (isset($res->errcode) && $res->errcode==0){
            return $res->behavior_data;
        }else{
            return [];
        }
    }
    /**
     * 获取「群聊数据统计」数据   按群主聚合的方式
     *

    参数	必须	说明
    day_begin_time	是	起始日期的时间戳，填当天的0时0分0秒（否则系统自动处理为当天的0分0秒）。取值范围：昨天至前180天。
    day_end_time	否	结束日期的时间戳，填当天的0时0分0秒（否则系统自动处理为当天的0分0秒）。取值范围：昨天至前180天。如果不填，默认同 day_begin_time（即默认取一天的数据）
    owner_filter	是	群主过滤。如果不填，表示获取应用可见范围内全部群主的数据（但是不建议这么用，如果可见范围人数超过1000人，为了防止数据包过大，会报错 81017）
    owner_filter.userid_list	是	群主ID列表。最多100个
    order_by	否	排序方式。1 - 新增群的数量 2 - 群总数 3 - 新增群人数4 - 群总人数 默认为1
    order_asc	否	是否升序。0-否；1-是。默认降序
    offset	否	分页，偏移量, 默认为0
    limit	否	分页，预期请求的数据量，默认为500，取值范围 1 ~ 1000
     *
     *
    此接口查询的时间范围为 [day_begin_time, day_end_time]，前后均为闭区间（即包含day_end_time当天的数据），支持的最大查询跨度为30天；
    用户最多可获取最近180天内的数据（超过180天企业微信将不再存储）；
    当传入的时间不为0点时，会向下取整，如传入1554296400(Wed Apr 3 21:00:00 CST 2019)会被自动转换为1554220800（Wed Apr 3 00:00:00 CST 2019）;
     * */
    public function groupchat_statistic(array $userid_list=[],$start_time='',$end_time=null,$all = false,$offset=0,$limit=1000)
    {
        $url = "https://qyapi.weixin.qq.com/cgi-bin/externalcontact/groupchat/statistic?access_token={$this->getToken()}";
        $res = Http::post($url, [
            'day_begin_time'=>$start_time,
            'day_end_time'=>$end_time,
            'owner_filter'=>[
                'userid_list'=>$userid_list
            ],
            'offset'=>$offset,
            'limit'=>$limit,

        ]);

        if (isset($res->errcode) && $res->errcode==0){
            if ($all){
                $list = $res->items ?? [];
                if ($res->total == $res->next_offset){
                    return  $res->items;
                }
                $offset = $offset + $limit;
                return array_merge($list,$this->groupchat_statistic($userid_list,$start_time,$end_time,$all,$offset,$limit));
            }else{
                return $res->items;
            }

        }else{
            return [];
        }
    }
    /**
     * 获取「群聊数据统计」数据   按群主聚合的方式
     *

    参数	必须	说明
     *

    owner_filter	是	群主过滤。
    如果不填，表示获取应用可见范围内全部群主的数据（但是不建议这么用，如果可见范围人数超过1000人，为了防止数据包过大，会报错 81017）
    owner_filter.userid_list	是	群主ID列表。最多100个
    day_begin_time	是	起始日期的时间戳，填当天的0时0分0秒（否则系统自动处理为当天的0分0秒）。取值范围：昨天至前180天。
    day_end_time	否	结束日期的时间戳，填当天的0时0分0秒（否则系统自动处理为当天的0分0秒）。取值范围：昨天至前180天。如果不填，默认同 day_begin_time（即默认取一天的数据）
    owner_filter	是	群主过滤。如果不填，表示获取应用可见范围内全部群主的数据（但是不建议这么用，如果可见范围人数超过1000人，为了防止数据包过大，会报错 81017）
    owner_filter.userid_list	是	群主ID列表。最多100个
    order_by	否	排序方式。1 - 新增群的数量 2 - 群总数 3 - 新增群人数4 - 群总人数 默认为1
    order_asc	否	是否升序。0-否；1-是。默认降序
    offset	否	分页，偏移量, 默认为0
    limit	否	分页，预期请求的数据量，默认为500，取值范围 1 ~ 1000
     *
     *
    此接口查询的时间范围为 [day_begin_time, day_end_time]，前后均为闭区间（即包含day_end_time当天的数据），支持的最大查询跨度为30天；
    用户最多可获取最近180天内的数据（超过180天企业微信将不再存储）；
    当传入的时间不为0点时，会向下取整，如传入1554296400(Wed Apr 3 21:00:00 CST 2019)会被自动转换为1554220800（Wed Apr 3 00:00:00 CST 2019）;
     * */
    public function groupchat_statistic_group_by_day(array $userid_list=[],$start_time='',$end_time=null)
    {
        $url = "https://qyapi.weixin.qq.com/cgi-bin/externalcontact/groupchat/statistic_group_by_day?access_token={$this->getToken()}";
        $res = Http::post($url, [
            'day_begin_time'=>$start_time,
            'day_end_time'=>$end_time,
            'owner_filter'=>[
                'userid_list'=>$userid_list
            ],
        ]);
        if (isset($res->errcode) && $res->errcode==0){
            return $res->items;
        }else{
            return [];
        }
    }

    /**
     *
    start_time	是	朋友圈记录开始时间。Unix时间戳
    end_time	是	朋友圈记录结束时间。Unix时间戳
    creator	否	朋友圈创建人的userid
    filter_type	否	朋友圈类型。0：企业发表 1：个人发表 2：所有，包括个人创建以及企业创建，默认情况下为所有类型
    cursor	否	用于分页查询的游标，字符串类型，由上一次调用返回，首次调用可不填
    limit	否	返回的最大记录数，整型，最大值20，默认值20，超过最大值时取默认值

     * @return array
     */
    public function get_moment_list($start_time,$end_time,$creator=null,$filter_type=null,$is_all = false,$limit=20,$cursor=null)
    {
        $url = "https://qyapi.weixin.qq.com/cgi-bin/externalcontact/get_moment_list?access_token={$this->getToken()}";
        $data = [
            'start_time'=>$start_time,
            'end_time'=>$end_time,
        ];
        if ($creator){$data['creator']= $creator;}
        if ($filter_type){$data['filter_type']= $filter_type;}
        if ($limit){$data['limit']= $limit;}
        if ($cursor){$data['cursor']= $cursor;}

        $res = Http::post($url, $data);
        if (isset($res->errcode) && $res->errcode==0){
            if ($is_all){
                $list = $res->moment_list ?? [];
                $next_cursor = $res->next_cursor ?? null;
                if ($list && $next_cursor){
                    return array_merge($list,$this->get_moment_list($start_time,$end_time,$creator,$filter_type,$is_all ,$limit,$next_cursor));
                }else{
                    return $list;
                }
            }else{
                return $res->moment_list;
            }
        }else{
            return [];
        }

    }
}