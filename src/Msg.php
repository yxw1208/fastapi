<?php


namespace Fastapi\Qywx;


/**
 * 消息的相关操作
 * */
class Msg extends BaseQwApi
{
    //发送消息到企业微信
    /**
     * $msg 消息体
     * to 接受人
     *  [
     *          touser   成员ID列表（消息接收者，最多支持1000个）。每个元素的格式为： corpid/userid，其中，corpid为该互联成员所属的企业，userid为该互联成员所属企业中的帐号。如果是本企业的成员，则直接传userid即可
     *          toparty  部门ID列表，最多支持100个。partyid在互联圈子内唯一。每个元素都是字符串类型，格式为：linked_id/party_id，其中linked_id是互联id，party_id是在互联圈子中的部门id。如果是本企业的部门，则直接传party_id即可。
     *          totag    本企业的标签ID列表，最多支持100个。
     *  ]
     * */
    public function SendMsg($msg,$to,&$res = null){
        $url = "https://qyapi.weixin.qq.com/cgi-bin/message/send?access_token={$this->getToken()}";
        $touser = $to['touser'] ?? null;
        $totag = $to['totag'] ?? null;
        $toparty = $to['toparty'] ?? null;
        if ($touser){
            $msg['touser'] = implode('|',$touser);
        }
        if ($totag){
            $msg['totag'] = implode('|',$totag);
        }
        if ($toparty){
            $msg['toparty'] = implode('|',$toparty);
        }
        $res = Http::post($url,$msg);
        if ( isset($res->errcode) && $res->errcode == 0 ){
            return true;
        }
        return  false;
    }
    /** *文本消息
     * @params  $content  消息内容
     *          $agentid  企业应用appid
     *          $toall   1表示发送给应用可见范围内的所有人（包括互联企业的成员），默认为0
     * */
    public function MakeText($content,$agentid,$toall=0){

        $data = [
            "msgtype" => "text",
            "agentid" => $agentid,
            "text" => [
                "content" => $content
            ],
            'toall'=>$toall,
            "safe"=>0,   //表示是否是保密消息，0表示否，1表示是，默认0
            "enable_id_trans"=> 0,
            "enable_duplicate_check"=> 0,
            "duplicate_check_interval"=> 1800
        ];
        return $data;
    }
    /** *文本消息
     * @params array  $article    图文消息，一个图文消息支持1到8条图文
     *                      title           标题，不超过128个字节，超过会自动截断
     *                      description     描述，不超过512个字节，超过会自动截断
     *                      url             点击后跳转的链接。
     *                      picurl         图文消息的图片链接，支持JPG、PNG格式，较好的效果为大图 640x320，小图80x80
     *                      btntxt          按钮文字，仅在图文数为1条时才生效。 默认为“阅读全文”， 不超过4个文字，超过自动截断。该设置只在企业微信上生效，微工作台（原企业号）上不生效。
     *                  例：
     *                  [
    [
    "title" => "中秋节礼品领取",
    "description" => "今年中秋节公司有豪礼相送",
    "url" => "URL",
    "picurl" => "http://res.mail.qq.com/node/ww/wwopenmng/images/independent/doc/test_pic_msg1.png",
    "btntxt"=>"更多"
    ]
    ]
     *
     *          $agentid  企业应用appid
     *          $toall   1表示发送给应用可见范围内的所有人（包括互联企业的成员），默认为0
     * */
    public function MakeNews($articles,$agentid,$toall=0){
        return [
            'toall'=>$toall,
            'msgtype'=>'news',
            'agentid'=>$agentid,
            'news'=>[
                'articles'=>$articles
            ],
        ];

    }
    /**
     * 生成小程序通知消息
     * */
    public function MakeMiniprogramNotice($appid,$page,$title,$description,$content_item){
        return [
            'msgtype'=>'miniprogram_notice',
            'miniprogram_notice'=>[
                'appid'=>$appid,
                'page'=>$page,
                'title'=>$title,    //  消息标题，长度限制4-12个汉字（支持id转译）
                'description'=>$description,
                'emphasis_first_item'=>true,  //是否放大第一个content_item
                'content_item'=>$content_item
            ],
            'enable_id_trans'=>0,           // 表示是否开启id转译，0表示否，1表示是，默认0
            'enable_duplicate_check'=>0,   // 表示是否开启重复消息检查，0表示否，1表示是，默认0
            'duplicate_check_interval'=>1800  //表示是否重复消息检查的时间间隔，默认1800s，最大不超过4小时
        ];

    }
    /**
     *  卡片消息 图文
     * @param $agentid 应用id
    jump_list	否	跳转指引样式的列表，该字段可为空数组，但有数据的话需确认对应字段是否必填，列表长度不超过3
    jump_list.type	否	跳转链接类型，0或不填代表不是链接，1 代表跳转url，2 代表跳转小程序
    jump_list.title	是	跳转链接样式的文案内容，建议不超过18个字
    jump_list.url	否	跳转链接的url，jump_list.type是1时必填
    jump_list.appid	否	跳转链接的小程序的appid，必须是与当前应用关联的小程序，jump_list.type是2时必填
    jump_list.pagepath	否	跳转链接的小程序的pagepath，jump_list.type是2时选填
     *
     *
     * horizontal_content_list.type	否	链接类型，0或不填代表不是链接，1 代表跳转url，2 代表下载附件，3 代表点击跳转成员详情
    horizontal_content_list.keyname	是	二级标题，建议不超过5个字
    horizontal_content_list.value	否	二级文本，如果horizontal_content_list.type是2，该字段代表文件名称（要包含文件类型），建议不超过30个字
    horizontal_content_list.url	否	链接跳转的url，horizontal_content_list.type是1时必填
    horizontal_content_list.media_id	否	附件的media_id，horizontal_content_list.type是2时必填
    horizontal_content_list.userid	否	成员详情的userid，horizontal_content_list.type是3时必填
     * */
    public function MakeTemplateCardnNewsNotice($agentid,$title,$desc,$card_image,$type,$url,$appid,$pagepath,$source=[],$vertical_content_list=[],$jump_list=[]){
        return [
            'agentid'=>$agentid,
            'msgtype'=>'template_card',
            'template_card'=>[
                'card_type'=>'news_notice',  //模板卡片类型，文本通知型卡片填写 “text_notice”  图文展示型  news_notice
                'source'=>$source,   //卡片头部
                'main_title'=>[
                    'title'=>$title,
                    'desc'=>$desc
                ],
                'card_image'=>[
                    'url'=>$card_image
                ],
                'vertical_content_list'=>$vertical_content_list, // 卡片二级垂直内容，列表长度不超过4  {title,desc}
                'jump_list'=>$jump_list, // 列表长度不超过6
                'card_action'=>[
                    'type'=>$type,
                    'url'=>$url,
                    'appid'=>$appid,
                    'pagepath'=>$pagepath,
                ],

            ],
            'enable_id_trans'=>0,           // 表示是否开启id转译，0表示否，1表示是，默认0
            'enable_duplicate_check'=>0,   // 表示是否开启重复消息检查，0表示否，1表示是，默认0
            'duplicate_check_interval'=>1800  //表示是否重复消息检查的时间间隔，默认1800s，最大不超过4小时
        ];

    }
    /**
     *  卡片消息 文本通知型
     * @params int     $agentid     应用ID
     * @params array   $main_title   一级标题，建议不超过36个字，文本通知型卡片本字段非必填，但不可本字段和sub_title_text都不填，（支持id转译）
     * @params string  $sub_title_text 二级标题  不可与$main_title 同时为空
     * @params array   $main_title    卡片整体跳转配置
     * @params array   $source        卡片头部
     * @params array   $quote_area    引用文献样式
     * @params array   $horizontal_content_list    二级标题+文本列表，该字段可为空数组
     * @params array   $jump_list                   跳转指引样式的列表，该字段可为空数组，但有数据的话需确认对应字段是否必填，列表长度不超过3
     *
     * @params array   $action_menu    卡片头部 交互列表 需要配置应用回调
     * @params string  $task_id        任务id，同一个应用任务id不能重复，只能由数字、字母和“_-@”组成，最长128字节，填了action_menu字段的话本字段必填
     * */
    public function makeTemplateCardTextNotice($agentid,$main_title=[],$sub_title_text=null,$card_action, $source = [],$quote_area=[],$emphasis_content=[],$horizontal_content_list=[],$jump_list=[], $action_menu = [],$task_id = null)
    {
        if (empty($main_title) && empty($sub_title_text)){
            return [];
        }
        $data =  [
            'agentid'=>$agentid,
            'msgtype'=>'template_card',
            'enable_id_trans'=>0,           // 表示是否开启id转译，0表示否，1表示是，默认0
            'enable_duplicate_check'=>0,   // 表示是否开启重复消息检查，0表示否，1表示是，默认0
            'duplicate_check_interval'=>1800  //表示是否重复消息检查的时间间隔，默认1800s，最大不超过4小时
        ];
        $template_card = [
            'card_type'=>'text_notice',
            'main_title'=>[
                "title"=>$main_title['title'] ?? '',
                "desc"=>$main_title['desc'] ?? '',
            ],
            'task_id'=>$task_id,
            'sub_title_text'=>$sub_title_text,
            'card_action'=>$card_action,
            'source'=>[ //卡片头部
                "icon_url"=>$source['icon_url'] ?? '',   // 图片的url',
                "desc"=>$source['desc'] ?? '',        // 企业微信',
                "desc_color"=>$source['desc_color'] ?? 1,   // 来源文字的颜色，目前支持：0(默认) 灰色，1 黑色，2 红色，3 绿色
            ],
        ];
        //  引用样式
        if (!empty($quote_area)){
//            'quote_area'=>[
//                'type'=>1,
//                'url'=>'https://work.weixin.qq.com',
//                'title'=>'企业微信的引用样式',
//                'quote_text'=>'企业微信真好用呀真好用',
//            ],
            $template_card['quote_area'] = $quote_area;
        }
        // 主体内容
        if (!empty($emphasis_content)){
//            'emphasis_content'=>[
//                'title'=>'100',
//                'desc'=>'核心数据',
//            ],
            $template_card['emphasis_content'] = $emphasis_content;
        }
        // 二级列表
        if (!empty($horizontal_content_list)){
//            'horizontal_content_list'=>[
//                [
//                    'type'=>1,
//                    'keyname'=>'企业微信官网',
//                    'value'=>'点击访问',
//                    'url'=>'https://work.weixin.qq.com'
//                ],
//            ],
            $template_card['horizontal_content_list'] = $horizontal_content_list;
        }
        // 跳转列表
        if (!empty($jump_list)){
//            'jump_list'=>[
//                [
//                    'title'=>'企业微信官网',
//                ],
//                [
//                    'type'=>1,
//                    'title'=>'企业微信官网',
//                    'url'=>'https://work.weixin.qq.com'
//                ],
//            ],
            $template_card['jump_list'] = $jump_list;
        }
        // 回调列表
        if (!empty($action_menu)){
//            'action_menu'=>[ // 不必须 卡片头部  右边的点
//                "desc"=>$action_menu['desc'] ?? '',                     // 卡文本说明',
//                "action_list"=>$action_menu['action_list'] ?? [],        // [{"text": "接受推送", "key": "A"} {"text": "不再推送", "key": "B"}]
//            ],
            $template_card['action_menu'] = $action_menu;
        }
        $data['template_card'] = $template_card;
        return  $data;
    }

    /**
     * Notes: 创建文件消费
     * @param $media_id
     * @param $agentid
     * @param int $safe 否	表示是否是保密消息，0表示可对外分享，1表示不能分享且内容显示水印，默认为0
     * @param int $enable_duplicate_check 否	表示是否开启重复消息检查，0表示否，1表示是，默认0
     * @param int $duplicate_check_interval 否	表示是否重复消息检查的时间间隔，默认1800s，最大不超过4小时
     * @return array
     *
     */
    public function makeFile($media_id,$agentid,$safe=0,$enable_duplicate_check=0,$duplicate_check_interval=1800){
        $data = [
            "msgtype" => "file",
            "agentid" => $agentid,
            "file" => [
                "media_id" => $media_id
            ],
            "safe"=>$safe,   //表示是否是保密消息，0表示否，1表示是，默认0
            "enable_duplicate_check"=> $enable_duplicate_check,
            "duplicate_check_interval"=> $duplicate_check_interval
        ];
        return $data;
    }

    /** *文本消息
     * @params  $content  消息内容
     *          $agentid  企业应用appid
     *          $toall   1表示发送给应用可见范围内的所有人（包括互联企业的成员），默认为0
     * */
    public function makemarkdown($content,$agentid,$toall=0){
        $data = [
            "msgtype" => "markdown",
            "agentid" => $agentid,
            "markdown" => [
                "content" => $content
            ],
            "enable_duplicate_check"=> 0,
            "duplicate_check_interval"=> 1800
        ];
        return $data;
    }
}