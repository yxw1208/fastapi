<?php


namespace Fastapi\Qywx;


/**
 * 企微标签相关
 * */
class Tag extends BaseQwApi
{
    /**
     *获取客户列表
     * $userid  企业成员的userid
     */
    public function getAll($group_id=[],$tag_id=[],&$res=null)
    {
        $url = "https://qyapi.weixin.qq.com/cgi-bin/externalcontact/get_corp_tag_list?access_token={$this->getToken()}";
        $data = [];
        if ($group_id){
            $data['group_id'] = $group_id;
        }
        if ($tag_id){
            $data['tag_id'] = $tag_id;
        }
        $res = Http::post($url, $data);
        $code = $res->errcode ?? 1;
        if ($code == 0) {
            return $res->tag_group;
        } else {
            return false;
        }
    }

    public function update($userid,$external_userid,$add_tag,$remove_tag=[],&$res=null)
    {
        $url = "https://qyapi.weixin.qq.com/cgi-bin/externalcontact/mark_tag?access_token={$this->getToken()}";
        $data = [
            'userid'=>$userid,
            'external_userid'=>$external_userid,
            'add_tag'=>$add_tag,
            'remove_tag'=>$remove_tag
        ];
        $res = Http::post($url, $data);
        $code = $res->errcode ?? 1;
        if ($code == 0) {
            return true;
        } else {
            return false;
        }
    }
}