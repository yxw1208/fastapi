<?php


namespace Fastapi\Qywx;



class Upload extends BaseQwApi
{


    /**
     * 上传临时素材
    素材上传得到media_id，该media_id仅三天内有效
    media_id在同一企业内应用之间可以共享

    参数	        必须	        说明
    access_token	是	        调用接口凭证
    type	        是	        媒体文件类型，分别有图片（image）、语音（voice）、视频（video），普通文件（file）
    file            是          '@'.dirname($_SERVER["DOCUMENT_ROOT"].$_SERVER["REQUEST_URI"]).'/file.jpg'
     * */
    public function media($file,$filename='',$type='file',&$res=null){
        $url = "https://qyapi.weixin.qq.com/cgi-bin/media/upload?access_token={$this->getToken()}&type=$type";
        $data = array();
        $data["media"] = '@'.$file;
        if (!empty($filename)){
            $data['filename'] = $filename;
        }
        $res = Http::post_file($url,$data);
        if (isset($res->errcode)&&$res->errcode==0){
            return $res->media_id;
        }else{
            return false;
        }
    }





}