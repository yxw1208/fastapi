<?php


namespace Fastapi\Qywx;


class Member extends BaseQwApi
{
    /**根据前台code 获取
     * 获取身份信息ID
     * */
    public function getUserID($code,&$res=null)
    {
        $url = "https://qyapi.weixin.qq.com/cgi-bin/user/getuserinfo?access_token={$this->getToken()}&code=$code";
        $res = Http::get($url);
        if (isset($res->errcode) && $res->errcode == 0) {
            return $res->UserId;
        } else {
            return false;
        }
    }
    /**
     *在通讯录同步助手中此接口可以读取企业通讯录的所有成员信息，而自建应用可以读取该应用设置的可见范围内的成员信息。
     *userid    成员UserID。对应管理端的帐号，企业内必须唯一。不区分大小写，长度为1~64个字节
     *
     */
    public function getUserInfo($userid,&$res=null)
    {
        $url = "https://qyapi.weixin.qq.com/cgi-bin/user/get?access_token={$this->getToken()}&userid=$userid";
        $res = Http::get($url);
        if (isset($res->errcode) && $res->errcode == 0) {
            return $res;
        } else {

            if (isset($res->errcode) && $res->errcode == 60111) {
                //第三方应用文档才有  根据手机号获取userid
                $url = "https://qyapi.weixin.qq.com/cgi-bin/user/getuserid?access_token={$this->getToken()}";
                $data = ['mobile' => $userid];
                $res = Http::post($url, $data);

                if ($res->errcode == 0) {
                    return $this->GetUserInfo($res->userid);
                } else {
                    return false;
                }
            } else {
//                $msg = "\n" . date('Y-m-d H:i:s') . ' USERID ：【' . $userid . '】 获取用户信息失败 ' . "\n";
//                $msg .= '请求URL : ' . request()->url(true) . "\n";
//                $msg .= '企业微信接口返回结果 : ' . json_encode($res, JSON_UNESCAPED_UNICODE) . "\n";
//                $msg .= '----------------------------------' . "\n";
//                Http::WriteFile(date('d') . '.log', $msg, $this->logPath);
            }

            return false;
        }


    }
    /**
     * 根据userid获取openid
     * @param $userid
     * @return false
     */
    public function convert_to_openid($userid,&$res=null)
    {
        $url = "https://qyapi.weixin.qq.com/cgi-bin/user/convert_to_openid?access_token={$this->getToken()}";
        $data = ['userid' => $userid];
        $res = Http::post($url, $data);
        if (isset($res->errcode) && $res->errcode == 0) {
            return $res->openid;
        }
        return false;
    }
    /**
     * 获取部门成员
     * 参数    必须    说明
     * access_token    是    调用接口凭证
     * department_id    是    获取的部门id
     * fetch_child    否    是否递归获取子部门下面的成员：1-递归获取，0-只获取本部门
     * */
    public function getSimplelist($department_id, $fetch_child = 0,&$res=null)
    {
        $url = "https://qyapi.weixin.qq.com/cgi-bin/user/simplelist?access_token={$this->getToken()}&department_id=$department_id&fetch_child=$fetch_child";
        $res = Http::get($url);
        if (isset($res->errcode) && $res->errcode == 0) {
            return $res->userlist;
        }

        return false;

    }
    /**
     * 更新成员数据
     * @param $data
     * @return false|mixed|string
     */
    public function updated($data,&$res=null)
    {

        $url = "https://qyapi.weixin.qq.com/cgi-bin/user/update?access_token={$this->getToken()}";

        $res = Http::post($url, $data);

        if (isset($res->errcode) && $res->errcode == 0) {

            return $res;

        }

        return false;
    }
    /**
     *批量删除成员
     *{
     * "useridlist": ["zhangsan", "lisi"]
     * }
     */
    public function batchdelete($userList,&$res=null)
    {
        $url = "https://qyapi.weixin.qq.com/cgi-bin/user/batchdelete?access_token={$this->getToken()}";
        $data = [
            'useridlist' => $userList
        ];
        $res = Http::post($url, $data);
        if (isset($res->errcode) && $res->errcode == 0) {
            return $res;
        }
        return false;
    }
    /**
     * 获取标签列表
     * @param $tagid
     * @return false|mixed|string
     */
    public function getTagList(&$res=null)
    {
        $url = "https://qyapi.weixin.qq.com/cgi-bin/tag/list?access_token={$this->getToken()}";
        $res = Http::get($url);
        if (isset($res->errcode) && $res->errcode == 0) {
            return $res->taglist;
        }
        return false;
    }
    /**
     * 获取标签成员
     * @param $tagid
     * @return false|mixed|string
     */
    public function getTagUsers($tagid,&$res=null)
    {
        $url = "https://qyapi.weixin.qq.com/cgi-bin/tag/get?access_token={$this->getToken()}&tagid={$tagid}";
        $res = Http::get($url);
        if (isset($res->errcode) && $res->errcode == 0) {
            return $res;
        }
        return false;
    }
    /**
     * 增加标签成员
     *  access_token    是    调用接口凭证
     * tagid    是    标签ID
     * userlist    否    企业成员ID列表，注意：userlist、partylist不能同时为空，单次请求个数不超过1000
     * partylist    否    企业部门ID列表，注意：userlist、partylist不能同时为空，单次请求个数不超过100
     * */
    public function addTagUsers($tagid, $userlist = [], $partlist = [],&$res=null)
    {
        if (empty($userlist) && empty($partlist)) {
            return false;
        }
        $url = "https://qyapi.weixin.qq.com/cgi-bin/tag/addtagusers?access_token={$this->getToken()}";
        $data = [
            'tagid' => $tagid,
            'userlist' => $userlist,
            'partylist' => $partlist
        ];
        $res = Http::post($url, $data);
        if (isset($res->errcode) && $res->errcode == 0) {
            return $res;
        }
        return false;

    }
    /**
     * 删除标签成员
    tagid	是	标签ID
    userlist	否	企业成员ID列表，注意：userlist、partylist不能同时为空，单次请求长度不超过1000
    partylist	否	企业部门ID列表，注意：userlist、partylist不能同时为空，单次请求长度不超过100
     * */
    public function delTagUsers($tagid, $userlist = [], $partylist = [],&$res=null)
    {
        if (empty($userlist) && empty($partylist)) {
            return false;
        }
        $url = "https://qyapi.weixin.qq.com/cgi-bin/tag/deltagusers?access_token={$this->getToken()}";
        $data = [
            'tagid' => $tagid,
            'userlist' => $userlist,
            'partylist' => $partylist
        ];
        $res = Http::post($url, $data);
        if (isset($res->errcode) && $res->errcode == 0) {
            return $res;
        }
        return false;

    }

}